class ChessBoardController < ApplicationController
  def index
  	@row_counter = 8

  	@chessBoard = Array.new(8) {Array.new(8)}

  	@chessBoard.each_index do |x|
  		subarray = @chessBoard[x]
  		subarray.each_index do |y|
  			
  			if x.even? and y.even?
  				@chessBoard[x][y] = 'black'
  			elsif x.even? and y.odd?
  				@chessBoard[x][y] = 'white'
  			end

  			if x.odd? and y.even?
  					@chessBoard[x][y] = 'white'
  			elsif x.odd? and y.odd?
  					@chessBoard[x][y] = 'black'
  			end

  		end
  	end
  end
end
